======================
inotifyx Release Notes
======================

.. contents::


inotifyx 0.2.0 2011-07-09
=========================

 * The distutils option "download_url" is now specified.  This should fix
   problems using pip, zc.buildout, and other tools that rely on PyPI for
   package downloads to install inotifyx.  Thanks to Dariusz Suchojad for
   the report.
   (Forest Bond)

 * inotifyx is now distributed as a Python package instead of a few bare
   modules.  The "_inotifyx" C extension is now shipped as "inotifyx.binding".
   This change should be backwards compatible with respect to imports since
   the C extension did not directly implement public interfaces.
   (Forest Bond)

 * A __version__ attribute is now provided.  This specifies the version
   of the inotifyx library.  Thanks to Jérôme Laheurte for the suggestion.
   (Forest Bond)


inotifyx 0.1.2 2011-03-31
=========================

 * Threads are now allowed during I/O operations.  Thanks to Jean-Baptiste
   Denis for the report and patch.
   (Forest Bond)


inotifyx 0.1.1 2009-10-18
=========================

 * Fix integer truncation of timeout argument in get_events.  Thanks to Eric
   Firing for the report and patch.
   (Forest Bond)


inotifyx 0.1.0 2009-04-26
=========================

 * Initial release.
   (Forest Bond)
